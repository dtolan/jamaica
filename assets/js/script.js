$(document).on("click touchstart", ".modal-open", function() {

	var packageName = $(this).data( 'package' );
	var packageId = '#' + packageName;

	$( packageId ).toggleClass( 'is-open' );
	$( 'body' ).toggleClass( 'no-event' );

	console.log(packageId)

});

$(document).on("click touchstart", ".modal-close", function() {

	var packageName = $(this).data( 'package' );
	var packageId = '#' + packageName;

	$( packageId ).toggleClass( 'is-open' );
	$( 'body' ).toggleClass( 'no-event' );

	console.log(packageId)

});
