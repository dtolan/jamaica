// Define amrDefaultApp
angular.module('amrSmoothCarousel',
    ['ngResource', 'ngMessages', 'ngRoute']);

angular
    .module('amrSmoothCarousel')
    .factory('slideInfo', function($http) {
        return {
            get: function () {
                $http.get('slides.json'); // this will return a promise to controller
            }
        }
    });

angular
    .module('amrSmoothCarousel')
    .controller('SmoothCarouselController', function ($scope, $http, $sce) {
        var vm = this;

        $http({
            method : "GET",
            url : "slides.json"
        }).then(function(response) {
            vm.slides = response.data.slides;
            for (var x in vm.slides)
                vm.slides[x].copy = $sce.trustAsHtml(vm.slides[x].copy);
        });

        vm.currentSlide = 1;

        vm.lastSlide = function () {
            if (vm.currentSlide == 1)
                return vm.slides.length;
            else
                return +vm.currentSlide - 1;
        };

        vm.nextSlide = function () {
            if (vm.currentSlide == vm.slides.length)
                return 1;
            else
                return +vm.currentSlide + 1;
        };

        vm.goForward = function () {
            vm.currentSlide = vm.nextSlide();
        };

        vm.goBackward = function () {
            vm.currentSlide = vm.lastSlide();
        };

        vm.getStateClass = function (slide) {
            var baseClass = 'amr-smooth-carousel__slide';

            if (slide.no == vm.nextSlide())
                return baseClass + '--offset-right';
            else if (slide.no == vm.lastSlide())
                return baseClass + '--offset-left';
            else if (slide.no != vm.currentSlide)
                return baseClass + '--hidden';
        };

        vm.style = function (slide) {

        };
    });

// Define amrDefaultApp routing
angular.
	module('amrSmoothCarousel').
	config(['$locationProvider', '$routeProvider',
		function config($locationProvider, $routeProvider) {
		    // Routing Config
		}
	]);

// Placeholder for non-angular JS code
