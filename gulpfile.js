var gulp        = require('gulp');
var plugins     = require('gulp-load-plugins')({
          pattern: ['gulp-*', 'gulp.*', 'merge-stream', 'main-bower-files', 'browser-sync'],
    replaceString: /\bgulp[\-.]/
});
var browserSync = plugins.browserSync.create();
var del         = require('del');

// Sass
gulp.task('sass', function () {
    return gulp.src('assets/sass/**/*.sass')
        .pipe(plugins.sass({
            outputStyle: 'compressed'
        }).on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer({
            browsers: ['> 1%','last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({stream: true}));
});

// Javascript
gulp.task('js', function () {
    return gulp.src('assets/js/**/*.js')
        // .pipe(plugins.uglify())
        .pipe(gulp.dest('dist/js'));
});

// Imgae Optimization
gulp.task('imagemin', function() {
    return gulp.src('assets/images/**/*')
        .pipe(plugins.imagemin())
        .pipe(gulp.dest('dist/images'))
        .pipe(browserSync.reload({stream: true}));
});

// Compile Bower CSS
gulp.task('bowerCSS', function () {
    lessStream = gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.filter('**/*.less'))
        .pipe(plugins.less());
    sassStream = gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.filter(['**/*.sass','**/*.scss']))
        .pipe(plugins.sass());
    cssStream = gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.filter('**/*.css'));
    return fullStream = plugins.mergeStream(lessStream, sassStream, cssStream)
        .pipe(plugins.concat('bower.css'))
        .pipe(gulp.dest('dist/css/lib'));
})

// Compile Bower JS
gulp.task('bowerJS', function () {
    gulp.src(plugins.mainBowerFiles())
        .pipe(plugins.filter('**/*.js'))
        .pipe(plugins.concat('bower.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest('dist/js/lib'));
})

// Start a server with Browser Sync
gulp.task('browser-sync', ['build'], function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

// Watch files and auto-reload with Browser Sync
gulp.task('watch', ['browser-sync'], function() {
    gulp.watch('assets/sass/**/*.sass', ['sass']);
    gulp.watch('assets/js/**/*.js', ['js']);
    gulp.watch('assets/images/**/*', ['imagemin']);
    gulp.watch('bower_components', ['bowerCompile']);
    gulp.watch('*.html').on('change', browserSync.reload);
});

gulp.task('clean', function () {
    return del('dist/**/*');
});

// Default task definitions
gulp.task('bowerCompile',['bowerJS','bowerCSS']);
gulp.task('build', ['sass','js','bowerCompile','imagemin']);
gulp.task('serve', ['browser-sync']);
gulp.task('default', ['build']);
